import IPCemitter from "./IPCemitter.js";
import createBroker from "./createBroker.js";

try {
  if (!process.send) throw Error("This script is intended to be started through a \"fork\" call")

  if (process.env.VERBOSE_RUNNER) console.log(`[${process.pid}] process forked from ${process.ppid} using startRunner.js`)

  let emitter = new IPCemitter(process)

  emitter.request(`configuration`)
    .then(conf => createBroker(conf))
    .then(broker => {

      /**
       * Create API listeners
       */
      emitter.onRequest(`broker.property`, property => broker[property])

      // forwards all events
      broker.onEvent(ctx => emitter.send("forwardEvent", ctx))

      //Starting the runner, and send the response when done
      emitter.onRequestAsync(`broker.start`, () => broker.start())

      //When running, allow the parent to stop the node
      emitter.onRequestAsync(`broker.stop`, () => broker.stop())

      //allow parent to log
      emitter.onRequestAsync(`broker.log`, ({ level, args }) => broker.logger[level](...args))

      //allow parent to make broker mcall
      emitter.onRequestAsync(`broker.call`, ({ action, params, options }) => broker.call(action, params, options))

      //allow parent to make broker mcall
      emitter.onRequestAsync(`broker.mcall`, (actions) => broker.mcall(actions))

      //allow parent to make broker emit
      emitter.onRequestAsync(`broker.emit`, ({ event, payload, options }) => broker.emit(event, payload, options))

      //allow parent to make broker broadcast
      emitter.onRequestAsync(`broker.broadcast`, ({ event, payload, options }) => broker.broadcast(event, payload, options))

      emitter.onRequestAsync(`broker.waitForServices`, ({ serviceNames, timeout, interval }) => broker.waitForServices(serviceNames, timeout, interval))

      /**
       * notify the parent process that we are ready to run
       */
      emitter.send("ready")
    })
} catch (err) {
  console.log(err)
  throw err
}