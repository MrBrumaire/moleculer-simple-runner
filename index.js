import Path from "path"
import URL from "url"
import { fork } from "child_process"
import { Runner } from "./src/runner.js"
import createBroker from "./src/createBroker.js"
import IPCemitter from "./src/IPCemitter.js"

/* beautify preserve:start */
//find absolute script path // this is the ugliest hack of the history of js hacks
const dirpath = Path.dirname(URL.fileURLToPath(import.meta.url))
/* beautify preserve:end */

/**
 *
 * Run a moleculer broker with given config
 *
 * @export
 * @param {Object} [config={}] - runner configuration object
 * @param {boolean?} config.MOLECULER_REPL_MODE - enter repl mode on 
 * @param {string?} config.SERVICES_DIR - directories to explore for services, separated by ":"
 * @param {string?} config.SERVICES - services in previous directories to exec, will enable whitelist mode
 * @param {string?} config.EXCLUDED_SERVICES - service to exclude from execution
 * @param {string?} config.BROKER_CONFIG - configuration of the broker
 * @param {string|object|null} config.RUNNER_ENV - env file or object
 * @param {object|null} config.RUNNER_ENV_OVERRIDES - additionnal environment
 * @param {boolean} [config.verbose=false] - verbose mode
 * @param {string|null} [forkID=null] - fork the process
 * 
 * @return {Runner} 
 * 
 * @example let runner = createRunner()
 */
export default async function createRunner(config, forkID = false) {

  if (!forkID) {
    // Create the broker on the same process
    let broker = await createBroker(config)
    return new Runner(broker)
  } else {
    const NODE_ID = typeof forkID === "string" ? forkID : undefined

    // Create the broker on a child process
    let child = fork(`${dirpath}/src/startRunner.js`, {
      cwd: ".",
      env: {
        ...process.env,
        NODE_ID,
        VERBOSE_RUNNER: config.verbose
      }
    })
    process.on("SIGTERM", () => child.kill())
    process.on("SIGINT", () => child.kill())
    process.on("exit", () => child.kill())

    let ipcEmitter = new IPCemitter(child)

    ipcEmitter.onRequest(`configuration`, () => config)

    return new Runner(ipcEmitter)
  }
}